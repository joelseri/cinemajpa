package fr.laerce.cinema.web;


import fr.laerce.cinema.dao.PersonneDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

//TODO séparer les acteurs des réalisateurs en typant les personnes, conditionner les controlleurs en fonction du type

@Controller
public class MainController {

    @Autowired
    PersonneDao personneDao;

    @GetMapping("/")
    public String main(Model model) {
        model.addAttribute("nom", "Cyril");
        return "index";
    }

    @GetMapping("/liste-films")
    public String listeFilms(Model model) {
        model.addAttribute("listP", personneDao.getAll());
        return "liste-personnes";
    }



    // Version avec @RequestParam
//    @GetMapping("/film")
//    public String detail(Model model, @RequestParam("id") String id) {
//        Integer idFilm = Integer.parseInt(id);
//        model.addAttribute("film", filmsDao.getById(idFilm));
//        return "detail";
//    }


}
